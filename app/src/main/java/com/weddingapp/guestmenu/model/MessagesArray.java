
package com.weddingapp.guestmenu.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MessagesArray {

    @SerializedName("guestmessages")
    @Expose
    private List<Guestmessage> guestmessages = null;
    @SerializedName("success")
    @Expose
    private String success;

    public List<Guestmessage> getGuestmessages() {
        return guestmessages;
    }

    public void setGuestmessages(List<Guestmessage> guestmessages) {
        this.guestmessages = guestmessages;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

}
