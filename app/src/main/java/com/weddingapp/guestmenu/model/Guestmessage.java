
package com.weddingapp.guestmenu.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Guestmessage {
    @SerializedName("guest_index")
    @Expose
    private String guestIndex;
    @SerializedName("guest_family_name")
    @Expose
    private String guestFamilyName;
    @SerializedName("guest_surname")
    @Expose
    private String guestSurname;
    @SerializedName("guest_message")
    @Expose
    private String guestMessage;
    public String getGuestIndex() {
        return guestIndex;
    }
    public void setGuestIndex(String guestIndex) {
        this.guestIndex = guestIndex;
    }
    public String getGuestFamilyName() {
        return guestFamilyName;
    }

    public void setGuestFamilyName(String guestFamilyName) {
        this.guestFamilyName = guestFamilyName;
    }

    public String getGuestSurname() {
        return guestSurname;
    }

    public void setGuestSurname(String guestSurname) {
        this.guestSurname = guestSurname;
    }

    public String getGuestMessage() {
        return guestMessage;
    }

    public void setGuestMessage(String guestMessage) {
        this.guestMessage = guestMessage;
    }

}
