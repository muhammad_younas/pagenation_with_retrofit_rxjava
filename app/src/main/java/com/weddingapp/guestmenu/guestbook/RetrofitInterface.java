package com.weddingapp.guestmenu.guestbook;

import com.weddingapp.guestmenu.model.MessagesArray;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface RetrofitInterface {
    @GET("Wedding_App_Orders_Records/weddingo_nabeel_party/get_guest_messages.php?")
    Observable<MessagesArray> getMessages(@Query("start") Integer start,
                                          @Query("NumberOfRecords") Integer end);
}
