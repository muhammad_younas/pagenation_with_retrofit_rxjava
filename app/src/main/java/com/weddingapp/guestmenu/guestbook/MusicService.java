package com.weddingapp.guestmenu.guestbook;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.weddingapp.guestmenu.R;


public class MusicService  extends Service implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener,MediaPlayer.OnCompletionListener {
    private static final String ACTION_PLAY = "com.weddingapp.guestmenu.action.PLAY";
    public static boolean isServiceRunning = false;
    public static boolean isMusicPause = false;
    private static final String TAG = "MusicService";
    MediaPlayer mMediaPlayer = null;

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction().equals(ACTION_PLAY)) {
            mMediaPlayer = new MediaPlayer();
            try {
                if (mMediaPlayer.isPlaying()) {
                    mMediaPlayer.stop();
                    mMediaPlayer.release();
                    mMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.music);
                }else{
                    mMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.music);
                }
            } catch (Exception e) {
                Log.d(TAG,"Error-In-onStartCommand() :" + e.getMessage());
            }

            mMediaPlayer.setOnPreparedListener(this);
            mMediaPlayer.setOnCompletionListener(this);
            mMediaPlayer.setOnErrorListener(this);
        }
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    /** Called when MediaPlayer is ready */
    public void onPrepared(MediaPlayer player) {
        Log.d(TAG, "onPrepared() called with: player = [" + player + "]");
        isServiceRunning = true;
        player.start();
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        Log.d(TAG, "onError() called with: mediaPlayer = [" + mediaPlayer + "], i = [" + i + "], i1 = [" + i1 + "]");
        isServiceRunning = false;
        return false;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        isServiceRunning = false;
        if(mMediaPlayer.isPlaying()){
            mMediaPlayer.stop();
        }

        if (mMediaPlayer != null)
            mMediaPlayer.release();
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        if(mMediaPlayer==null){
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.music);
        }else{
            onPrepared(mMediaPlayer);
        }
    }
}
