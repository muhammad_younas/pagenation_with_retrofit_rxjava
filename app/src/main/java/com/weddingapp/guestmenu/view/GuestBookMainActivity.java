package com.weddingapp.guestmenu.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.weddingapp.guestmenu.R;
import com.weddingapp.guestmenu.guestbook.MusicService;
import com.weddingapp.guestmenu.guestbook.RetrofitInterface;
import com.weddingapp.guestmenu.guestbook.RxJavaBuilder;
import com.weddingapp.guestmenu.model.Guestmessage;
import com.weddingapp.guestmenu.model.MessagesArray;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.weddingapp.guestmenu.guestbook.MusicService.isServiceRunning;

public class GuestBookMainActivity extends AppCompatActivity {
    private RecyclerView reactiveList;
    private List<Guestmessage> mGuetMessagesList = new ArrayList<>();
    private Intent musicservice;
    private ProgressBar progressBar;
    private RecyclerView.OnScrollListener mScrollListner;
    private static final String TAG = "GuestBookMainActivity";
    private ReactiveRecyclerAdapter adapter    = new ReactiveRecyclerAdapter();
    private LinearLayoutManager layoutManager;
    private int start = 0;
    private int NumberOfRecords = 25;
    public static int scrollerSpeed = 0;
    public static boolean scrollingStatus = true;
    private boolean loading = true;
    private boolean isMoreData = true;
    private int end = NumberOfRecords;
    private Handler autoScrollerHandler;
    final int speedScroll = 1500;
    private RelativeLayout main_layout;
    private Runnable autoScrollerRunnable;

    public GuestBookMainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_book);
        reactiveList = (RecyclerView) findViewById(R.id.my_recycler_view);
        reactiveList.setHasFixedSize(true);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        layoutManager = new MyCustomLayoutManager(this);
        reactiveList.setLayoutManager(layoutManager);
        reactiveList.setAdapter(adapter);
        main_layout = (RelativeLayout) findViewById(R.id.main_layout);
        musicservice = new Intent(this,MusicService.class);
        musicservice.setAction("com.weddingapp.guestmenu.action.PLAY");
        startService(musicservice);
        getStarredRepos(true);




        mScrollListner = new RecyclerView.OnScrollListener() {
            int pastVisibleItems, visibleItemCount, totalItemCount;
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(dy > 0) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                    if (loading && isMoreData) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            loading = false;
                            isMoreData = false;
                            Log.d(TAG, "onScrolled() called ");
                            mGuetMessagesList.add(null);
                            getStarredRepos(false);
                        }
                    }
                }
            }
        };
        reactiveList.addOnScrollListener(mScrollListner);


//        reactiveList.setOnTouchListener(new View.OnTouchListener() {
//            float height;
//            @Override
//            public boolean onTouch(View view, MotionEvent event) {
//                int action = event.getAction();
//                float height = event.getY();
//                if(action == MotionEvent.ACTION_DOWN){
//                    this.height = height;
//                }else if(action == MotionEvent.ACTION_UP){
//                    if(this.height < height){
//                        scrollingStatus = true;
//                        //autoScrollerHandler.postDelayed(autoScrollerRunnable,speedScroll);
//                    }else if(this.height > height){
//                        scrollingStatus = true;
//                       // autoScrollerHandler.postDelayed(autoScrollerRunnable,speedScroll);
//                    }
//                }
//                return false;
//            }
//        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(isServiceRunning)
            stopService(musicservice);
    }

    private void getStarredRepos( boolean progress) {
        Log.d(TAG, "getStarredRepos() called with: start = [" + start + "], " +
                "end = [" + end + "], progress = [" + progress + "]");
        if(progress)
            progressBar.setVisibility(View.VISIBLE);
        RxJavaBuilder mTEmp = new RxJavaBuilder();
        RetrofitInterface service = mTEmp.mApi;
        service.getMessages(start,NumberOfRecords)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MessagesArray>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "In onCompleted()");
                        if(progress){
                            progressBar.setVisibility(View.GONE);
                           // startAutoScroll();
                        }
                        start = end +1;
                        end = end + NumberOfRecords;

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Log.d(TAG, e.getMessage());
                        if(progress)
                            progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onNext(MessagesArray messageslist) {
                        if(messageslist.getSuccess().equals("1")){
                            isMoreData = true ;
                            loading = true;
                            Log.d(TAG, "In onNext() getSuccess() : " +scrollerSpeed);
                            mGuetMessagesList.addAll(messageslist.getGuestmessages());
                            adapter.addAll(mGuetMessagesList);
                            mGuetMessagesList.clear();
                        }else{
                            loading = false;
                            isMoreData = false;
                        }
                    }
                });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.guest_book_menu, menu);
        return true;

    }

//    private void startAutoScroll(){
//
//        autoScrollerHandler = new Handler();
//        autoScrollerRunnable = new Runnable() {
//            @Override
//            public void run() {
//                //To check if  recycler is on bottom
//                Log.d(TAG,"scrollingStatus : " + scrollingStatus);
//                if(scrollingStatus){
//                    if(layoutManager.findLastVisibleItemPosition()==scrollerSpeed-1){
//                        Log.d(TAG,"onAutoScrollerComplete");
//                        reactiveList.scrollToPosition(0);
//                        reactiveList.smoothScrollToPosition(scrollerSpeed);
//                        autoScrollerHandler.postDelayed(this,speedScroll);
//                    }else{
//                        reactiveList.smoothScrollToPosition(scrollerSpeed);
//                        autoScrollerHandler.postDelayed(this,speedScroll);
//                    }
//                }else{
//                    return;
//                }
//            }
//        };
//        autoScrollerHandler.postDelayed(autoScrollerRunnable,speedScroll);
//    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        ActionMenuItemView musicIcon = (ActionMenuItemView)findViewById(R.id.music_icon);
        switch (item.getItemId()) {
            case R.id.music_icon:
               if(isServiceRunning) {
                   stopService(musicservice);
                   isServiceRunning = false;

//                   menuItem.setIcon(getResources().getDrawable(R.mipmap.m));
               }else {
                   startService(musicservice);
                   isServiceRunning = true;
               }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}