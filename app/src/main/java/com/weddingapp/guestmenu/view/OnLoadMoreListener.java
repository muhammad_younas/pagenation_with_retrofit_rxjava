package com.weddingapp.guestmenu.view;

/**
 * Created by Developer on 4/5/2017.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
