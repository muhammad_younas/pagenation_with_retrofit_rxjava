package com.weddingapp.guestmenu.view;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.weddingapp.guestmenu.R;
import com.weddingapp.guestmenu.model.Guestmessage;

import java.util.ArrayList;
import java.util.List;

public class ReactiveRecyclerAdapter extends RecyclerView.Adapter {
    private static final String TAG = "GuestBookMainActivity";
    private List<Guestmessage> items = new ArrayList<>();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    public void addAll(List<Guestmessage> moreItems) {
        items.addAll(moreItems);
        notifyDataSetChanged();
        Log.d(TAG, "getStarredRepos() ItemSize = [" + items.size() + "]");
        GuestBookMainActivity.scrollerSpeed = items.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyler_item, parent, false);
            vh = new ReactiveViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ReactiveViewHolder){
            Guestmessage item = items.get(position);
            ((ReactiveViewHolder)holder).family_name.setText(item.getGuestFamilyName());
            ((ReactiveViewHolder)holder).surname.setText(item.getGuestSurname());
            ((ReactiveViewHolder)holder).message.setText(item.getGuestMessage());
        }else if(holder instanceof ProgressViewHolder){
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }



    public static class ReactiveViewHolder extends RecyclerView.ViewHolder {

        TextView family_name;
        TextView surname;
        TextView message;
        public ReactiveViewHolder(View itemView) {
            super(itemView);
            family_name = (TextView) itemView.findViewById(R.id.family_name);
            surname = (TextView) itemView.findViewById(R.id.surname);
            message = (TextView) itemView.findViewById(R.id.message);

            itemView.setOnClickListener(view -> {
                Log.d(TAG,"click");
                GuestBookMainActivity.scrollingStatus = false;
            });
        }
    }


    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }
}